﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chess
{
    
    public partial class ConnectScreen : Page
    {
        private Frame frame;
        public ConnectScreen()
        {
            InitializeComponent();
            frame = MainWindow.frame;
        }
        private void MouseOver(object sender, DependencyPropertyChangedEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            var converter = new BrushConverter();
            if (!rect.IsMouseDirectlyOver)
                rect.Fill = (Brush)converter.ConvertFromString("#FF555555");
            else
            {
                rect.Fill = (Brush)converter.ConvertFromString("#FF444444");
            }

        }
        private void RectClick(object sender, MouseButtonEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            var converter = new BrushConverter();
            rect.Fill = (Brush)converter.ConvertFromString("#FF666666");
            rect.Visibility = Visibility.Hidden;
            ConnectGrid.Visibility = Visibility.Hidden;
            loading.Visibility = Visibility.Visible;
            IPbox.IsEnabled = false;
            PortBox.IsEnabled = false;
            int port = Convert.ToInt32(PortBox.Text);
            if (!TurnManager.Connect(IPbox.Text, port))
            {
                new Thread(() =>
                {
                    ShowLoading();
                }).Start();
            }
            else
            {
                new Thread(() =>
                {
                    WaitForConnect();
                }).Start();
            }
        }

        private void ShowLoading()
        {
            Thread.Sleep(1000);
            Application.Current.Dispatcher.Invoke(() =>
            {
                connectRect.Visibility = Visibility.Visible;
                ConnectGrid.Visibility = Visibility.Visible;
                loading.Visibility = Visibility.Hidden;
                IPbox.IsEnabled = true;
                PortBox.IsEnabled = true;
            });
        }
        private void WaitForConnect()
        {
            while (!GLobals.connected) { }
            GLobals.connected = false;
            Application.Current.Dispatcher.Invoke(() =>
            {
                frame.Content = null;
            });
        }
    }

}
