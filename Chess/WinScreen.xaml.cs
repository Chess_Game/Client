﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chess
{
    
    public partial class WinScreen : Page
    {
        private Frame frame;
        public WinScreen(string topText, string bottomText, bool forfeit = false)
        {
            InitializeComponent();
            frame = MainWindow.frame;
            Top.Text = topText;
            Bottom.Text = bottomText;
            if (forfeit)
            {
                RematchRect.Visibility = Visibility.Hidden;
                RematchGrid.Visibility = Visibility.Hidden;
            }
           
            System.Media.SoundPlayer player;
            player = new System.Media.SoundPlayer(Properties.Resources.checkmate);
            player.Play();
        }

       
        private void MouseOver(object sender, DependencyPropertyChangedEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            var converter = new BrushConverter();
            if (!rect.IsMouseDirectlyOver)
                rect.Fill = (Brush)converter.ConvertFromString("#FF555555");
            else
            {
                rect.Fill = (Brush)converter.ConvertFromString("#FF444444");
            }

        }

        private void RectClick(object sender, MouseButtonEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            var converter = new BrushConverter();
            rect.Fill = (Brush)converter.ConvertFromString("#FF666666");
            TurnManager.Send("Rematch");
            rect.Visibility = Visibility.Hidden;
            RematchGrid.Visibility = Visibility.Hidden;
            loading.Visibility = Visibility.Visible;
            new Thread(() =>
            {
                Reset();
            }).Start();
        }
        
        private void Reset()
        {
            while (!GLobals.rematch) { }
            Thread.Sleep(500);
            GLobals.rematch = false;
            TurnManager.turnNo = 0;
            GLobals.clientColor = GLobals.clientColor == Color.White ? Color.Black : Color.White;
            TurnManager.currentTurn = Color.White;
            Application.Current.Dispatcher.Invoke(() =>
            {
                foreach (Square sqr in Board.squareList)
                {
                    sqr.Piece = null;
                    sqr.Glow = 0;
                }
                Board.InitialPos();
                Board.ReverseBoard();
                frame.Content = null;
            });
        }
    }

}
