﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Chess
{
    public static class TurnManager
    {
        private static TcpClient client;
        private static Socket socket;
        private static UnicodeEncoding enc = new UnicodeEncoding();
        public static int turnNo = 0;
        public static Color currentTurn = Color.White;
        public static void Send(String message)
        {
            if (socket != null)
                socket.Send(enc.GetBytes(message));
        }
        public static void NextTurn(Move moveMade = null)
        {
            if(moveMade != null)
            {
                string jsonMove = Newtonsoft.Json.JsonConvert.SerializeObject(moveMade);
                if(socket != null)
                    socket.Send(enc.GetBytes(jsonMove));
                
            }
            turnNo++;
            Color prevTurn;
            if (currentTurn == Color.White)
            {
                currentTurn = Color.Black;
                prevTurn = Color.White;
            }
            else
            {
                currentTurn = Color.White;
                prevTurn = Color.Black;
            }
            if(IsCheckTurn() != null)
            {
                IsCheckTurn().square.Glow = 1;
                IsCheckTurn().Checked = true;
                if (CheckMate())
                {
                    MainWindow.frame.Content = new WinScreen("Checkmate", String.Format("{0} is victorious", prevTurn.ToString()));
                    if (socket != null)
                    {
                        socket.Send(enc.GetBytes("Checkmate"));
                    }
                }
                else
                    Console.WriteLine("not checkmate");
            }
            else
            {
                if (CheckMate()) // No available move and king is not in check
                {
                    MainWindow.frame.Content = new WinScreen("Stalemate", "½ - ½");
                    if (socket != null)
                    {
                        socket.Send(enc.GetBytes("Stalemate"));
                    }
                }
            }

        }
        public static King IsCheckTurn()
        {
            King checkedKing = null;
            if (GLobals.WhiteKing.IsInCheck())
                checkedKing = GLobals.WhiteKing;

            if (GLobals.BlackKing.IsInCheck())
                checkedKing = GLobals.BlackKing;
            return checkedKing;
        }
        public static bool CheckMate()
        {
            foreach (Square square in Board.boardArray)
            {
                if(square.Piece != null && square.Piece.color == currentTurn)
                {
                    foreach (int[] move in square.Piece.GenerateMoves(square))
                    {
                        if (!square.Piece.WillBeInCheck(square.location, move))
                            return false;
                    }
                }
            }
            return true;
        }

        public static bool Connect(string ip = "localhost", int port = 8080)
        {
            try
            {
                Console.WriteLine("Connecting...");
                client = new TcpClient(ip, port);
                socket = client.Client;
                Console.WriteLine("Connected");
                Task.Factory.StartNew(() =>
                {
                    Receive();
                });
                return true;
            }
            catch
            {
                Console.WriteLine("Failed to connect");
                return false;
            }
        }
        private static void Receive()
        {
            bool firstRun = true;
            while (true)
            {
                try
                {
                    byte[] receiveBuffer = new byte[1024];
                    int bytesRec = socket.Receive(receiveBuffer);
                    Array.Resize(ref receiveBuffer, bytesRec);
                    string receivedString = Encoding.Unicode.GetString(receiveBuffer);
                    if (firstRun)
                    {
                        firstRun = false;
                        Enum.TryParse(receivedString, out GLobals.clientColor);
                        if (GLobals.clientColor == Color.Black)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                Board.ReverseBoard();
                            });
                        }
                        GLobals.connected = true;
                    }
                    else
                    {
                        Color prevTurn = currentTurn == Color.White ? Color.Black : Color.White;
                        if (receivedString == "Checkmate")
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                MainWindow.frame.Content = new WinScreen("Checkmate", String.Format("{0} is victorious", prevTurn.ToString()));
                            });
                        }
                        else if (receivedString == "Rematch")
                        {
                            GLobals.rematch = true;
                            Console.WriteLine(GLobals.rematch);
                        }
                        else if(receivedString == "Stalemate")
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                MainWindow.frame.Content = new WinScreen("Stalemate", "½ - ½");
                            });
                        }
                        else if(receivedString == "Forfeit")
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                MainWindow.frame.Content = new WinScreen("Forfeit",String.Format("{0} quit the game", prevTurn.ToString()), true);
                            });
                        }
                        else
                        {
                            Console.WriteLine("received: {0}", receivedString);
                            var move = Newtonsoft.Json.JsonConvert.DeserializeObject<Move>(receivedString);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (move.Promoted)
                                    Board.boardArray[move.Org[0], move.Org[1]].Piece.ValidMove(move.Org, move.Dest, true, move.PromotedTo);
                                else
                                    Board.boardArray[move.Org[0], move.Org[1]].Piece.ValidMove(move.Org, move.Dest, true);
                            });
                            NextTurn();
                        }
                        
                    }
                }
                catch
                {
                    Console.WriteLine("Connection ended");
                }
            }
        }
    }

    public class Move
    {
        public int[] Org { get; set; }
        public int[] Dest { get; set; }
        public bool Promoted { get; set; }
        public int PromotedTo { get; set; }
    }

}
