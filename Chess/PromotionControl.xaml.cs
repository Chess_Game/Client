﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chess
{
    public partial class PromotionControl : UserControl
    {
        public static readonly DependencyProperty PColorProperty = DependencyProperty.Register
            ("PColor", 
            typeof(String), typeof(PromotionControl));

        public String PColor
        {
            get
            {
                return (String)GetValue(PColorProperty);
            }
            set
            {
                SetValue(PColorProperty, value);
            }
        }
        private static void SqrChanged(DependencyObject ctrl, DependencyPropertyChangedEventArgs eventArgs)
        {
            var control = (PromotionControl)ctrl;
            control.PSquare = (Square)eventArgs.NewValue;
        }
        public static readonly DependencyProperty PSquareProperty = DependencyProperty.Register
            ("PSquare", 
            typeof(Square), 
            typeof(PromotionControl), 
            new FrameworkPropertyMetadata(SqrChanged));
        public Square PSquare
        {
            get
            {
                return (Square)GetValue(PSquareProperty);
            }
            set
            {
                SetValue(PSquareProperty, value);
            }
        }
        public PromotionControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var color = GLobals.clientColor == Color.White ? "w" : "b";
            Piece[] pieceArray = {
                        new Queen(color),
                        new Knight(color),
                        new Rook(color),
                        new Bishop(color),
                         };
            var btn = (Button)sender;
            int x = 0;
            switch (btn.Name)
            {
                case "Queen":
                    x = 0;
                    break;
                case "Knight":
                    x = 1;
                    break;
                case "Rook":
                    x = 2;
                    break;
                case "Bishop":
                    x = 3;
                    break;
            }

            Button test = new Button();
            PSquare.Piece = pieceArray[x];
            Console.WriteLine(MainWindow.PCol.Count());
            MainWindow.PCol.RemoveAt(MainWindow.PCol.Count - 1);
            GLobals.pastMove.Promoted = true;
            GLobals.pastMove.PromotedTo = x;
            TurnManager.NextTurn(GLobals.pastMove);
            GLobals.promoting = false;
        }
    }
    public class Promotion
    {
        public int Row { get; set; }
        public string PClr { get; set; }
        public Square DestSquare { get; set; }
        public Promotion(int Row, Square DestSquare)
        {
            this.Row = Row;
            this.DestSquare = DestSquare;
            PClr = GLobals.clientColor == Color.White ? "w" : "b";
        }
            
    }
    public sealed class ImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            if (parameter == null || parameter.Equals(String.Empty)) parameter = "{0}";
            string path = String.Format((string)parameter, value);
            return new BitmapImage(new Uri(path, UriKind.Relative));
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
