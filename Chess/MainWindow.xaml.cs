﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chess
{
    public enum Color
    {
        None,
        White,
        Black
    }
    public static class GLobals
    {
        
        public static bool moving = false;
        public static bool promoting = false;
        public static bool rematch = false;
        public static bool connected = false;
        public static Move pastMove;
        public static Color clientColor = Color.None;
        public static Square squareToMove;
        public static GhostPawn EnPassantPawn;

        public static King WhiteKing;
        public static King BlackKing;

        public static Rook[] WhiteRooks = new Rook[2];
        public static Rook[] BlackRooks = new Rook[2];


    }
   
    public partial class MainWindow : Window
    {
        public static ObservableCollection<Square> squareList = new ObservableCollection<Square>();
        public static ObservableCollection<Promotion> PCol = new ObservableCollection<Promotion>();
        public static ItemsControl itemsControl;
        public static ItemsControl promotionCtrl;
        public static Frame frame;
      
        public MainWindow()
        {
            InitializeComponent();
            frame = Frame;
            Board.boardArray = new Square[8, 8];
            Board.squareList = squareList;
            itemsControl = XamlBoard;
            promotionCtrl = promControl;
            itemsControl.DataContext = squareList;
            promotionCtrl.DataContext = PCol;
            Board.DrawBoard();
            Board.InitialPos();
            Frame.Content = new ConnectScreen();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (GLobals.clientColor == TurnManager.currentTurn)
            {
                Button button = (Button)sender;
                Square clickedSqr = (Square)button.DataContext;
                //Console.WriteLine("[{0}]", string.Join(", ", clickedSqr.location));

                if (GLobals.moving)
                {
                    if (clickedSqr != GLobals.squareToMove)
                    {
                        foreach (var move in GLobals.squareToMove.Piece.GenerateMoves(GLobals.squareToMove))
                        {
                            //Console.WriteLine(String.Format("[{0}, {1}]", move[0],move[1]));
                            Board.boardArray[move[0], move[1]].Opacity = 0;

                        }
                        if (GLobals.squareToMove.Piece.ValidMove(GLobals.squareToMove, clickedSqr)) // Attempts to make the move
                        {
                            Move moveMade = new Move()
                            {
                                Org = GLobals.squareToMove.location,
                                Dest = clickedSqr.location
                            };
                            GLobals.moving = !GLobals.moving;
                            GLobals.pastMove = moveMade;
                            Console.WriteLine(GLobals.promoting);
                            if(!GLobals.promoting)
                                TurnManager.NextTurn(moveMade);
                            GLobals.squareToMove.Brush = Brushes.Black;
                        }
                        else
                        {
                            GLobals.squareToMove.Brush = Brushes.Black;
                            if (clickedSqr.Piece == null)
                                GLobals.moving = false;
                            else
                            {
                                GLobals.squareToMove.Brush = Brushes.Black;
                                GLobals.squareToMove = clickedSqr;
                                clickedSqr.Brush = Brushes.Red;
                                if (clickedSqr.Piece.color == GLobals.clientColor && !GLobals.promoting)
                                {
                                    foreach (var move in clickedSqr.Piece.GenerateMoves(clickedSqr))
                                    {
                                        if (!clickedSqr.Piece.WillBeInCheck(clickedSqr.location, move))
                                            Board.boardArray[move[0], move[1]].Opacity = .25;
                                    }
                                }
                            }
                        }
                    }

                }
                else
                {
                    if (clickedSqr.Piece != null)
                    {
                        GLobals.moving = !GLobals.moving;
                        GLobals.squareToMove = clickedSqr;
                        clickedSqr.Brush = Brushes.Red;
                        if (clickedSqr.Piece.color == GLobals.clientColor && !GLobals.promoting)
                        {
                            foreach (var move in clickedSqr.Piece.GenerateMoves(clickedSqr))
                            {
                                //Console.WriteLine(String.Format("[{0}, {1}]", move[0],move[1]));
                                if (!clickedSqr.Piece.WillBeInCheck(clickedSqr.location, move))
                                    Board.boardArray[move[0], move[1]].Opacity = .25;
                            }
                        }
                    }
                }
            }
        }
    }

    public class Square : INotifyPropertyChanged
    {
        private string _PieceImg { get; set; }
        private Piece _Piece { get; set; }
        private Brush _Brush { get; set; }
        private double _Opacity { get; set; }
        private int _Glow { get; set; }
        public Brush Color { get; set; }
        public int[] location = new int[2];
        public double Opacity
        {
            get { return _Opacity; }
            set
            {
                if (_Opacity != value)
                {
                    _Opacity = value;
                    OnPropertyChanged("Opacity");
                }
            }
        }
        public int Glow
        {
            get { return _Glow; }
            set
            {
                if (_Glow != value)
                {
                    _Glow = value;
                    OnPropertyChanged("Glow");
                }
            }
        }
        public Brush Brush
        {
            get { return _Brush; }
            set
            {
                if (_Brush != value)
                {
                    _Brush = value;
                    OnPropertyChanged("Brush");
                }
            }
        }
        public string PieceImg
        {
            get { return _PieceImg; }
            set
            {
                if (_PieceImg != value)
                {
                    _PieceImg = value;
                    OnPropertyChanged("PieceImg");
                }
            }
        }
        public Piece Piece
        {
            get { return _Piece; }
            set
            {
                if (_Piece != value)
                {
                    _Piece = value;
                    if(value != null)
                    {
                        this.PieceImg = _Piece.image;
                        OnPropertyChanged("Piece");
                    }
                    else
                    {
                        this.PieceImg = "\\Images\\Transparent.png";
                    }
                }
            }
        }

        public Square(Brush Color, Piece Piece, int[] location)
        {
            this.Color = Color;
            this.Piece = Piece;
            this.location = location;
            Brush = Brushes.Black;
            Opacity = 0;
            Glow = 0;
            if(Piece == null)
            {
                PieceImg = "\\Images\\Transparent.png";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }

    public static class Board
    {
        public static Square[,] boardArray;
        public static ObservableCollection<Square> squareList;
        public static void DrawBoard()
        {
            bool swap = false;
            var converter = new BrushConverter();
            Brush color;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (j % 8 == 0)
                    {
                        swap = !swap;
                    }
                    if (swap)
                    {
                        color = (Brush)converter.ConvertFromString("#899DB5"); // black
                        swap = !swap;
                    }
                    else
                    {
                        color = (Brush)converter.ConvertFromString("#6E8694"); //white
                        swap = !swap;
                    }
                    Square sqr = new Square(color, null, new[] { i, j });
                    squareList.Add(sqr);
                    boardArray[i,j] = sqr;
                }
            }
        }
        private static readonly string white = "w";
        private static readonly string black = "b";
        public static void InitialPos()
        {
            for(int i = 0; i < 8; i++)
            {
                InsertPiece(1, i, new Pawn(black));
                InsertPiece(6, i, new Pawn(white));
            }

            GLobals.BlackKing = new King(black);
            GLobals.WhiteKing = new King(white);
            GLobals.BlackRooks[0] = new Rook(black);
            GLobals.BlackRooks[1] = new Rook(black);
            GLobals.WhiteRooks[0] = new Rook(white);
            GLobals.WhiteRooks[1] = new Rook(white);

            InsertPiece(0, 4, GLobals.BlackKing);
            InsertPiece(7, 4, GLobals.WhiteKing);

            InsertPiece(0, 0, GLobals.BlackRooks[0]);
            InsertPiece(0, 1, new Knight(black));
            InsertPiece(0, 2, new Bishop(black));
            InsertPiece(0, 3, new Queen(black));
            InsertPiece(0, 5, new Bishop(black));
            InsertPiece(0, 6, new Knight(black));
            InsertPiece(0, 7, GLobals.BlackRooks[1]);

            InsertPiece(7, 0, GLobals.WhiteRooks[0]);
            InsertPiece(7, 1, new Knight(white));
            InsertPiece(7, 2, new Bishop(white));
            InsertPiece(7, 3, new Queen(white));
            InsertPiece(7, 5, new Bishop(white));
            InsertPiece(7, 6, new Knight(white));
            InsertPiece(7, 7, GLobals.WhiteRooks[1]);



        }
        public static Square Coord(int x, int y)
        {
            return boardArray[x-1, y-1];
        }

        public static void InsertPiece(int[] pos, Piece piece)
        {
            boardArray[pos[0], pos[1]].Piece = piece;
        }
        public static void InsertPiece(int x, int y, Piece piece)
        {
            boardArray[x,y].Piece = piece;
        }

        public static Piece GetPieceAt(int[] pos)
        {
            return boardArray[pos[0], pos[1]].Piece;
        }
        public static Piece GetPieceAt(int x, int y)
        {
            return boardArray[x, y].Piece;
        }

        public static void ReverseBoard()
        {

            int size = squareList.Count() - 1;
            for (int i = 0; size > i; size--, i++)
            {
                var temp = squareList[size];
                squareList[size] = squareList[i];
                squareList[i] = temp;
            }
            
        }

    }

}
