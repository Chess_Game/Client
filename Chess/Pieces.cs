﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Media;
using System.Diagnostics;


namespace Chess
{
    public abstract class Piece
    {
        
        public Color color;
        public bool isCaptured = false;
        public string image;
        public void SetColor(string color)
        {
            if (color == "w")
            {
                this.color = Color.White;
            }
            else if (color == "b")
            {
                this.color = Color.Black;
            }
        }
        public bool WithinBounds(Square dest)
        {
            int x = dest.location[0]; int y = dest.location[1];
            if(x >= 0 && y >= 0 && x < 8 && y < 8)
            {
                return true;
            }
            return false;
        }
        public bool WithinBounds(int x, int y)
        {
            if(x >= 0 && y >= 0 && x < 8 && y < 8)
            {
                return true;
            }
            return false;
        }
        public abstract List<int[]> GenerateMoves(Square org, bool pass = false);
        public bool ValidMove(Square org, Square dest, bool received = false, int promoted = -1)
        {
            if (!received && ( !WithinBounds(dest) || org.Piece.color != GLobals.clientColor || GLobals.promoting))
            {
                return false;
            }
            foreach (var move in GenerateMoves(org))
            {
                if (move.SequenceEqual(dest.location))
                {
                    
                    Piece pToMove = this;
                    if (WillBeInCheck(org.location, dest.location))
                        return false;
                    
                    Move(pToMove, org, dest);
                    if(received && promoted >= 0)
                    {
                        var color = GLobals.clientColor == Color.White ? "b" : "w";
                        Piece[] pieceArray = {
                        new Queen(color),
                        new Knight(color),
                        new Rook(color),
                        new Bishop(color),
                         };
                        dest.Piece = pieceArray[promoted];
                    }
                    if(GLobals.EnPassantPawn != null)
                    {
                        if (GLobals.EnPassantPawn.realSquare.Piece is GhostPawn)
                            GLobals.EnPassantPawn.realSquare.Piece = null;
                        GLobals.EnPassantPawn = null;
                    }
                    if (this is Pawn pawn)
                    {
                        if (pawn.firstMove)
                            pawn.firstMove = false;
                        if(Math.Abs(dest.location[0] - org.location[0]) == 2)
                        {
                            GLobals.EnPassantPawn = new GhostPawn(pawn, dest);
                        }
                        if (!received && (dest.location[0] == 0 || dest.location[0] == 7))
                        {
                            Move moveMade = new Move()
                            {
                                Org = org.location,
                                Dest = dest.location
                            };
                            GLobals.promoting = true;
                            int[] a = { 7, 6, 5, 4, 3, 2, 1, 0 }; //Mirror the position for when board is flipped
                            if(GLobals.clientColor == Color.White)
                                MainWindow.PCol.Add(new Promotion(dest.location[1], dest));
                            else 
                                MainWindow.PCol.Add(new Promotion(a[dest.location[1]], dest));
                        }
                    }
                    if (this is Rook rook)
                        rook.hasMoved = true;
                    if (this is King king)
                    {
                        king.hasMoved = true;
                        king.square = dest;
                        int distance = dest.location[1] - org.location[1];
                        if (Math.Abs(distance) > 1)
                        {
                            if (distance < 0)
                            {
                                var temp = Board.boardArray[org.location[0], 0].Piece;
                                Board.boardArray[org.location[0], 0].Piece = null;
                                Board.boardArray[org.location[0], dest.location[1] + 1].Piece = temp;
                            }
                            if (distance > 0)
                            {
                                var temp = Board.boardArray[org.location[0], 7].Piece;
                                Board.boardArray[org.location[0], 7].Piece = null;
                                Board.boardArray[org.location[0], dest.location[1] - 1].Piece = temp;
                            }
                        }
                    }
                    if (TurnManager.IsCheckTurn() == null)
                    {
                        GLobals.BlackKing.square.Glow = 0;
                        GLobals.WhiteKing.square.Glow = 0;
                    }
                    return true;
                }
            }
            return false;
        }
        public void ValidMove (int[] org, int[] dest, bool received = false, int promoted = -1)
        {
            ValidMove(Board.boardArray[org[0], org[1]], Board.boardArray[dest[0], dest[1]], received, promoted);
        }
        public void Move(Piece pieceToMove, Square org, Square dest)
        {   
            Piece AttackedPiece = Board.GetPieceAt(dest.location);
            System.Media.SoundPlayer player;
            player = new System.Media.SoundPlayer(Properties.Resources.move);
            if (AttackedPiece != null)
            {
                AttackedPiece.isCaptured = true;
                player = new System.Media.SoundPlayer(Properties.Resources.capture);
                if (AttackedPiece is GhostPawn gPawn && this is Pawn)
                {
                    gPawn.attachedPawn.isCaptured = true;
                    gPawn.attachedSquare.Piece = null;
                    GLobals.EnPassantPawn = null;
                }
            }
            player.Play();
            dest.Piece = org.Piece;
            org.Piece = null;
            
        }
        public bool IsJumpingPieces(int x, int y, int[] org)
        {
            int dirX = x - org[1];
            if(dirX > 0)
            {
                dirX = 1;
            }
            else if(dirX < 0)
            {
                dirX = -1;
            }
            int dirY = y - org[0];
            if (dirY > 0)
            {
                dirY = 1;
            }
            else if (dirY < 0)
            {
                dirY = -1;
            }
            int[] pos = {org[1], org[0] };
            pos[0] += dirX;
            pos[1] += dirY;
            while (pos[0] != x || pos[1] != y)
            {
                if (Board.GetPieceAt(pos[1], pos[0]) != null)
                {
                    if(!(Board.GetPieceAt(pos[1], pos[0]).GetType() == typeof(GhostPawn)))
                    {
                        return true;

                    }
                }
                pos[0] += dirX;
                pos[1] += dirY;
                
            }
            return false;
        }
        public bool WillBeInCheck(int[] org, int[] dest)
        {
            //// Get call stack
            //StackTrace stackTrace = new StackTrace();

            //// Get calling method name
            //Console.WriteLine(stackTrace.GetFrame(1).GetMethod().Name);

            bool isCheck = false;
            if (this is King king)
            {
                isCheck = king.WillBeInCheck(org, dest);
            }
            else
            {
                King CurrentKing = TurnManager.currentTurn == Color.White ? GLobals.WhiteKing : GLobals.BlackKing;
                var tempPiece = Board.boardArray[dest[0], dest[1]].Piece;
                Board.boardArray[org[0], org[1]].Piece = null;
                Board.boardArray[dest[0], dest[1]].Piece = this;
                isCheck = CurrentKing.IsInCheck();
                Board.boardArray[org[0], org[1]].Piece = this;
                Board.boardArray[dest[0], dest[1]].Piece = tempPiece;
            }
            return isCheck;
        }
        public void AddToMoveList(Square org, int x, int y, ref List<int[]> moveList)
        {
            bool add = false;
            if (Board.GetPieceAt(y, x) == null)
                add = true;
            else if (this.color != Board.GetPieceAt(y, x).color)
                add = true;

            if (add)
                moveList.Add(new[] { y, x });
        }
    }

    public class King : Piece
    {
        public Square square;
        public bool hasMoved = false;
        public bool Checked = false;
        public King(string color)
        {
            base.SetColor(color);
            this.image = String.Format("\\Images\\K_{0}.png", color);
            square = this.color == Color.White ? Board.boardArray[7, 4] : Board.boardArray[0, 4];
        }

        public override List<int[]> GenerateMoves(Square org, bool pass = false)
        {
            List<int[]> moveList = new List<int[]>();
            
            for(int i = -1; i < 2; i++ )
            {
                for(int j = -1; j < 2; j++)
                {
                    int x = org.location[0] + i;
                    int y = org.location[1] + j;
                    if (WithinBounds(x, y))
                    {
                        if(i!= 0 || j != 0)
                        {
                            AddToMoveList(org, y, x, ref (moveList)); // this could be wrong (swap x and y)
                        }
                    }
                }

            }
            if (!pass)
            {
                if (CanCastle())
                    moveList.Add(new[] { org.location[0], 6 });
                if (CanCastle(false))
                    moveList.Add(new[] { org.location[0], 2 });
            }
            return moveList;
        }

        public bool IsInCheck(Square sqr = null) { 
        
            if(sqr == null)
                sqr = square;
            var selfColor = color == Color.White ? "w" : "b";
            var enemyColor = color == Color.Black ? "b" : "w";

            Piece[] pieceArray = {
                new Queen(selfColor),
                new Bishop(selfColor),
                new Rook(selfColor),
                new Knight(selfColor),
                new King(selfColor),
                new Pawn(enemyColor)
        };
            foreach (Piece pieceType in pieceArray)
            {
                foreach(int[] loc in pieceType.GenerateMoves(sqr, true))
                {
                    if (Board.GetPieceAt(loc) != null)
                    {
                        if (Board.GetPieceAt(loc).GetType() == pieceType.GetType())
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool CanCastle(bool Kingside = true) 
        {
            int j = color == Color.White ? 7 : 0;
            int i;
            bool rookMoved;
            bool isCheck;
            if(IsInCheck())
            {
                return false;
            }
            if (Kingside)
            {
                if (color == Color.White)
                    rookMoved = GLobals.WhiteRooks[1].hasMoved;
                else
                    rookMoved = GLobals.BlackRooks[1].hasMoved;
                if (hasMoved || rookMoved)
                    return false;
                for (i = square.location[1] + 1; i < 7; i++)
                {
                    if (Board.boardArray[j, i].Piece != null)
                        return false;
                    isCheck = WillBeInCheck(square.location, new int[] { j, i });
                    if (isCheck)
                        return false;
                }
            }
            else
            {
                if (color == Color.White)
                    rookMoved = GLobals.WhiteRooks[0].hasMoved;
                else
                    rookMoved = GLobals.BlackRooks[0].hasMoved;
                if (hasMoved || rookMoved)
                    return false;
                for (i = 0 + 1; i < 4; i++)
                {
                    if (Board.boardArray[j, i].Piece != null)
                        return false;
                    isCheck = WillBeInCheck(square.location, new int[] { j, i });
                    if (isCheck)
                        return false;
                }
            }
            return true;
        }

        public new bool WillBeInCheck(int[] org, int[] dest)
        {
            //// Get call stack
            //StackTrace stackTrace = new StackTrace();

            //// Get calling method name
            //Console.WriteLine(stackTrace.GetFrame(1).GetMethod().Name);

            bool isCheck;
            Square tempSquare = new Square(null, this, dest);
            Board.boardArray[org[0], org[1]].Piece = null;
            isCheck = IsInCheck(tempSquare);
            Board.boardArray[org[0], org[1]].Piece = this;
            return isCheck;
        }
    }

    public class  Queen : Piece
    {
        public Queen(string color)
        {
            base.SetColor(color);
            this.image = String.Format("\\Images\\Q_{0}.png", color);
        }

        public override List<int[]> GenerateMoves(Square org, bool pass = false)
        {
            List<int[]> moveList = new List<int[]>();

            // Horizontal Moves
            for (int i = 0; i < 8; i++)
            {
                int x = i;
                int y = org.location[0];
                if (x != org.location[1])
                {
                    if (!IsJumpingPieces(x, y, org.location))
                    {
                        AddToMoveList(org, x, y, ref (moveList));
                    }
                }
            }
        
            // Vertical Moves
            for (int i = 0; i < 8; i++)
            {
                int x = org.location[1];
                int y = i;
                if(y != org.location[0])
                {
                    if (!IsJumpingPieces(x, y, org.location)){
                        AddToMoveList(org, x, y, ref (moveList));
                    }
                }
            }

            // Diagonal Moves

            // From top left to bottom right
            for (int i = 0; i < 8; i++)
            {
                int x = i;
                int y = org.location[0] - (org.location[1] - i);
                if (x != org.location[1])
                {
                    if(WithinBounds(x, y))
                    {
                        if (!IsJumpingPieces(x, y, org.location))
                        {
                            AddToMoveList(org, x, y, ref (moveList));
                        }
                    }
                }

            }

            // From top right to bottom left
            for (int i = 0; i < 8; i++)
            {
                int x = org.location[1] + (org.location[0] - i);
                int y = i;
                if (x != org.location[1])
                {
                    if (WithinBounds(x, y))
                    {
                        if (!IsJumpingPieces(x, y, org.location))
                        {
                            AddToMoveList(org, x, y, ref (moveList));
                        }
                    }
                }

            }

            return moveList;
        }
    }
    
    public class Bishop : Piece
    {
        public Bishop(string color)
        {
            base.SetColor(color);
            this.image = String.Format("\\Images\\B_{0}.png", color);
        }

        public override List<int[]> GenerateMoves(Square org, bool pass = false)
        {
            List<int[]> moveList = new List<int[]>();

            // From top left to bottom right
            for (int i = 0; i < 8; i++)
            {
                int x = i;
                int y = org.location[0] - (org.location[1] - i);
                if (x != org.location[1])
                {
                    if (WithinBounds(x, y))
                    {
                        if (!IsJumpingPieces(x, y, org.location))
                        {
                            AddToMoveList(org, x, y, ref (moveList));
                        }
                    }
                }

            }

            // From top right to bottom left
            for (int i = 0; i < 8; i++)
            {
                int x = org.location[1] + (org.location[0] - i);
                int y = i;
                if (x != org.location[1])
                {
                    if (WithinBounds(x, y))
                    {
                        if (!IsJumpingPieces(x, y, org.location))
                        {
                            AddToMoveList(org, x, y, ref (moveList));
                        }
                    }
                }

            }

            return moveList;
        }
    }

    public class Rook : Piece
    {
        public bool hasMoved = false;
        public Rook(string color)
        {
            base.SetColor(color);
            this.image = String.Format("\\Images\\R_{0}.png", color);
        }

        public override List<int[]> GenerateMoves(Square org, bool pass = false)
        {
            List<int[]> moveList = new List<int[]>();

            // Horizontal Moves
            for (int i = 0; i < 8; i++)
            {
                int x = i;
                int y = org.location[0];
                if (x != org.location[1])
                {
                    if (!IsJumpingPieces(x, y, org.location))
                    {
                        AddToMoveList(org, x, y, ref (moveList));
                    }
                }
            }

            // Vertical Moves
            for (int i = 0; i < 8; i++)
            {
                int x = org.location[1];
                int y = i;
                if (y != org.location[0])
                {
                    if (!IsJumpingPieces(x, y, org.location))
                    {
                        AddToMoveList(org, x, y, ref (moveList));
                    }
                }
            }

            return moveList;
        }
    }
    
    public class Knight : Piece
    {
        public Knight(string color)
        {
            base.SetColor(color);
            this.image = String.Format("\\Images\\N_{0}.png", color);
        }

        public override List<int[]> GenerateMoves(Square org, bool pass = false)
        {
            List<int[]> moveList = new List<int[]>();
            // Sideways moves
            for (int i = -2; i < 3; i+=4)
            {
                for (int j = -1; j < 2; j += 2)
                {
                    int x = i + org.location[1];
                    int y = j + org.location[0];
                    if (WithinBounds(x, y))
                    {
                        AddToMoveList(org, x, y, ref (moveList));
                    }
                }
            }

            // Forward and backward
            for (int i = -1; i < 2; i += 2)
            {
                for (int j = -2; j < 3; j += 4)
                {
                    int x = i + org.location[1];
                    int y = j + org.location[0];
                    if (WithinBounds(x, y))
                    {
                        AddToMoveList(org, x, y, ref (moveList));
                    }
                }
            }

            return moveList;
        }
    }

    public class Pawn : Piece 
    {
        public bool firstMove = true;
        public bool enPassant = false;
        public Pawn(string color)
        {
            base.SetColor(color);
            this.image = String.Format("\\Images\\P_{0}.png", color);
        }

        public override List<int[]> GenerateMoves(Square org, bool pass = false)
        {
            List<int[]> moveList = new List<int[]>();
            int x, y;
            // Sideways moves
            for (int i = -1; i < 2; i+=2)
            {
                x = org.location[1] + i;
                if(this.color == Color.White)
                {
                    y = org.location[0] - 1;
                }
                else
                {
                    y = org.location[0] + 1;
                }
                if (WithinBounds(x, y))
                {
                    Piece attacking = Board.GetPieceAt(y, x);
                    if (attacking != null)
                    {
                        if (this.color != Board.GetPieceAt(y, x).color)
                        {
                            moveList.Add(new[] { y, x });
                        }
                    }
                }

            }

            // Front moves
            x = org.location[1];
            if(this.color == Color.White)
            {
                y = org.location[0] - 1;
            }
            else
            {
                y = org.location[0] + 1;
            }
            if(this.WithinBounds(x, y) && Board.GetPieceAt(y, x) == null)
            {
                moveList.Add(new[] { y, x });
            }

            // First move
            if (firstMove)
            {
                if(this.color == Color.White)
                {
                    y = org.location[0] - 2;
                }
                else
                {
                    y = org.location[0] + 2;
                }
                if (this.WithinBounds(x, y) && Board.GetPieceAt(y, x) == null)
                {
                    if (!IsJumpingPieces(x, y, org.location))
                    {
                        moveList.Add(new[] { y, x });
                    }
                }
            }

            return moveList;
        }
    }

    public class GhostPawn : Piece
    {
        public Pawn attachedPawn;
        public Square attachedSquare;
        public Square realSquare;
        private Color ghostColor;
        public GhostPawn(Pawn pawn, Square square)
        {
            image = "\\Images\\transparent.png";
            attachedPawn = pawn;
            attachedSquare = square;
            ghostColor = attachedPawn.color;
            int x = ghostColor == Color.White ? attachedSquare.location[0] + 1 : attachedSquare.location[0] - 1;
            int y = attachedSquare.location[1];
            realSquare = Board.boardArray[x, y];
            realSquare.Piece = this;

        }
        public override List<int[]> GenerateMoves(Square org, bool pass = false)
        {
            return new List<int[]>();
        }
    }

}
